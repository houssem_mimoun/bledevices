package com.example.myapplication4;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Set;

public class MainActivity extends AppCompatActivity {
    Button btnON, btnOFF, scanButton, discoverButton;
    BluetoothAdapter myBluetoothAdapter;
    BluetoothAdapter myAdapter = BluetoothAdapter.getDefaultAdapter();
    Intent btEnablingIntent;
    int requestCode;
    ListView listView, listView1;
    protected ArrayList<BluetoothDevice> foundDevices = new ArrayList<BluetoothDevice>();
    private ListView foundDevicesListView;
    ArrayAdapter<BluetoothDevice> btArrayAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnON = (Button) findViewById(R.id.btnON);
        btnOFF = (Button) findViewById(R.id.btnOFF);
        scanButton = (Button) findViewById(R.id.btnScan);
        listView = (ListView) findViewById(R.id.listView);
        discoverButton = (Button) findViewById(R.id.btnDiscover);
        foundDevicesListView = (ListView) findViewById(R.id.listView1);
        myBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        btEnablingIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        requestCode = 1;
        btArrayAdapter = new ArrayAdapter<BluetoothDevice>(this,
                android.R.layout.simple_list_item_1, foundDevices);

        foundDevicesListView.setAdapter(btArrayAdapter);

        int permissionCheck = this.checkSelfPermission("Manifest.permission.ACCESS_FINE_LOCATION");
        permissionCheck += this.checkSelfPermission("Manifest.permission.ACCESS_COARSE_LOCATION");
        if (permissionCheck != 0) {

            this.requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 1001); //Any number
        }

        clickBluetoothOn();
        clickBluetoothOff();
        scanDevices();
        discoverButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                btArrayAdapter.clear();

                myAdapter.startDiscovery();

                Toast.makeText(getApplicationContext(), "Scanning Devices", Toast.LENGTH_LONG).show();

            }
        });

        registerReceiver(FoundReceiver, new IntentFilter(BluetoothDevice.ACTION_FOUND));
        IntentFilter filter = new IntentFilter(
                BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        this.registerReceiver(FoundReceiver, filter);
    }

    private final BroadcastReceiver FoundReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            String action = intent.getAction();

            // When discovery finds a new device
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                // Get the BluetoothDevice object from the Intent
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                if (!foundDevices.contains(device)) {
                    String nT = device.getName();
                    foundDevices.add(device);
                    Toast.makeText(getApplicationContext(), "name: " + device.getName() + " " + device.getAddress(), Toast.LENGTH_LONG).show();
                    btArrayAdapter.notifyDataSetChanged();
                }

            }

            // When discovery cycle finished
            if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                if (foundDevices == null || foundDevices.isEmpty()) {
                    Toast.makeText(getApplicationContext(), "No Devices", Toast.LENGTH_LONG).show();
                }
            }

        }
    };

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        unregisterReceiver(FoundReceiver);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == requestCode) {
            if (resultCode == RESULT_OK) {
                Toast.makeText(getApplicationContext(), "Bluetooth is Enabled", Toast.LENGTH_LONG).show();
            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(getApplicationContext(), "Bluetooth is Cancelled", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void clickBluetoothOn(){
        btnON.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (myBluetoothAdapter == null) {
                    Toast.makeText(getApplicationContext(), "Bluetooth does not support this device", Toast.LENGTH_LONG).show();
                } else {
                    if (!myBluetoothAdapter.isEnabled()) {
                        startActivityForResult(btEnablingIntent, requestCode);
                    }
                }
            }
        });
    }


    private void clickBluetoothOff(){
        btnOFF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (myBluetoothAdapter.isEnabled()) {
                    myBluetoothAdapter.disable();
                }
            }
        });
    }

    private void scanDevices(){
        scanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Set<BluetoothDevice> setD = myBluetoothAdapter.getBondedDevices();
                String[] strings = new String[setD.size()];
                int i = 0;
                if (setD.size() > 0) {
                    for (BluetoothDevice device: setD) {
                        Log.d("Device:", "Device Name is:"+device.getName());
                        strings[i] = device.getName()+" "+device.getAddress();
                        i++;
                    }
                    ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, strings);
                    listView.setAdapter(arrayAdapter);
                }
            }
        });
    }



}





